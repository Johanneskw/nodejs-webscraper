const puppeteer = require('puppeteer');
const fs = require('fs');

function scrape(){
    (async () =>  {
        // url of page getting scraped. 
        let moviesUrl = 'https://www.imdb.com/chart/top/?ref_=nv_mv_250';
        // launching browser and opening page
        let browser = await puppeteer.launch();
        let page = await browser.newPage();
        // goes to page, and starts scraping. makes one array of titles and one of ratings, then makes objects of rating and title.
        await page.goto(moviesUrl, {waitUntil: 'networkidle2'});
        let data = await page.evaluate(() => {
            const titles = document.querySelectorAll('tbody[class="lister-list"] > tr > td.titleColumn');
            const ratings = document.querySelectorAll('tbody[class="lister-list"] > tr > td.ratingColumn.imdbRating strong');
            let moviesObjects = [];
            for (let i = 0; i < titles.length; i++) {
                moviesObjects[i] = {
                   title: titles[i].innerText,
                   rating: ratings[i].innerText
                }
            }
            // returns list of objects 
            return {
                moviesObjects
            }
        })

        console.log(data);
        writejson(data);

        await browser.close();
    })();
}
// Writes json file with all data. 
function writejson(content){
    fs.writeFile("output.json", JSON.stringify(content), function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    }); 
}

scrape();