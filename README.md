# Webscraper

Webscraper - imdb.

Simple webscraper which scrapes the rank, title and release year + imdb rating of all the top 250 rated movies on imdb. 
Using puppeteer to scrape the data, which is then written to a Json file: 'output.json'.


